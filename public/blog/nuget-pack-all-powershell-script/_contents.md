I've been creating NuGet packages for a while at work through our continues integration server. But I needed a way to quickly build and create NuGet packages locally. So, I've written a Power Shell script that will look for any and all NuSpec files recursively from current location, and then build and pack them into NuGet packages.

Here is the code in its entirety.

<pre>
<code class="powershell">$Location = Get-Location
$OutputDirectory="C:\Program Files (x86)\NuGet\Packages"
$Configuration = "Release"

Get-ChildItem -r *.nuspec | ForEach-Object {
    Set-Location $_.Directory
    NuGet Pack -OutputDirectory $OutputDirectory -Prop Configuration=$Configuration -Build
}

Set-Location $Location
</code>
</pre>

And then let's have quick walk through. First we need to remember our starting location, so that we can return to it later.

<pre>
<code class="powershell">$Location = Get-Location</code>
</pre>

Then we define where packages should be created and what configuration should be used to build the projects.

<pre><code class="powershell">$OutputDirectory="C:\Program Files (x86)\NuGet\Packages"
$Configuration = "Release"</code></pre>

We now get all the NuSpec files recursively and then loop over the results.

<pre><code class="powershell">Get-ChildItem -r *.nuspec | ForEach-Object {</code></pre>

Inside the loop we navigate to the directory of the NuSpec file.

<pre><code class="powershell">Set-Location $_.Directory</code></pre>

And then we invoke NuGet Pack with OutputDirectory and the Build Configuration as well as the Build flag in order to tell NuGet to build the project before creating the package.

<pre><code class="powershell">NuGet Pack -OutputDirectory $OutputDirectory -Prop Configuration=Release -Build</code></pre>

Then we exit the loop.

<pre><code class="powershell">}</code></pre>

And finally we clean up by navigating back to our starting location.

<pre><code class="powershell">Set-Location $Location</code></pre>

That is it. 

I hope you enjoyed this short article.

Kind regards,
Jonatan Pedersen