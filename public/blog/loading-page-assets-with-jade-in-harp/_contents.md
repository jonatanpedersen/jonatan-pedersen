While bundling assets is generally a good choice for layout, theming and site wide behaviour. I don't feel that it is appropriate for page specific styling and behaviour. Therefore, we need a way to define assets and control when they are loaded and in what order. We probably want to load site assets before page assets.

In other frameworks it's often possible to get this done by defining multiple placeholders in layout or master files and then have the page assets rendered in those placeholders.

In <a href="http://harpjs.com/">harp</a> however, its really only possible to define 1 placeholder in a <a href="http://harpjs.com/docs/development/layout">layout</a> (<code>yield</code>). So how do we achieve something like this in a <a href="http://jade-lang.com/">jade</a> layout?

<pre>
<code>doctype html
html(lang="en")
	head
		title="title"
		link(href="/css/main.css", rel="stylesheet")
		// Load page specific stylesheets here
	body
    	!= yield
		script(src="/js/main.js", type="text/javascript")
    	// Load page specific javascripts here
</code>
</pre>

Well, harp js, has a couple of concepts that we can use. We can start by creating a jade partial for stylesheets and one for javascripts with the following contents. Each of them iterates over an array and output link and script elements accordingly.

_stylesheets.jade
<pre>
<code>each stylesheet in stylesheets
	link(href=stylesheet, rel='stylesheet')
</code>
</pre>

_javascripts.jade
<pre>
<code>each javascript in javascripts
	script(src=javascript, type='text/javascript')
</code>
</pre>

Then we need to teach harp js that there is something called <code>stylesheets</code> and <code>javascripts</code>. We do that by adding the following two empty arrays to the globals object in a <code>harp.json</code>. These are default values and will also prevent "is not defined" errors on pages that don't have any page specific assets defined. 

<pre>
<code>{
	"globals": {
		...
		"stylesheets": [],
		"javascripts": []
	}
}
</code>
</pre>

Now we need to call the stylesheets and javascripts partials in <code>_layout.jade</code>.

<pre>
<code>doctype html
html(lang="en")
	head
		title title
		include ./_stylesheets.jade
	body
		!= yield
		include ./_javascripts.jade
		#footer
		p Copyright (c) foobar
</code>
</pre>

Add finally define page specific stylesheets and javascripts in <code>_data.json</code>.

<pre>
<code>{
	"contact": {
		...
		"stylesheets": [
			"contact.css"
		],
		"javascripts": [
			"/js/angular.min.js",
			"contact.js"
		]
	}
}</code>
</pre>

Thats it, we can now define and load page assets for each individual page in our harp application.

Kind regards,
Jonatan Pedersen

UPDATE: I've changed the layout template to use jades built-in include function instead of Harps partial() function. Include is compile time and partial() is runtime, and partial() creates a dependency on Harp in the jade template, which makes it less portable. Thanks to <a href="https://twitter.com/ForbesLindesay">‏@ForbesLindesay</a> and <a href="https://twitter.com/rob_ellis">@Rob Ellis</a> for feedback.