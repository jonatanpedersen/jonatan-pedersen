If you have ever tried renaming a solution and changing its root namespace in Visual Studio, you will know that it is quite cumbersome and non-trivial at best. 

Consider the following Visual Studio solution:

<figure>
  <ul class="file-list">
    <li><i class="fa fa-folder-o"></i> SolutionA
      <ul>
        <li><i class="fa fa-file-o"></i> SolutionA.sln</li>
        <li><i class="fa fa-file-o"></i> SolutionA.vs12.suo</li>
        <li><i class="fa fa-folder-o"></i> SolutionA
          <ul>
            <li><i class="fa fa-file-o"></i> SolutionA.csproj</li>
          </ul>
        </li>
        <li><i class="fa fa-folder-o"></i> SolutionA.Test
          <ul>
            <li><i class="fa fa-file-o"></i> SolutionA.Test.csproj</li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
</figure>

Using the rename feature in Visual Studio will only change the name of the solution file. 

Renaming "SolutionA" to "SolutionB" will therefore result in the following:

<figure>
  <ul class="file-list">
    <li><i class="fa fa-folder-o"></i> SolutionA
      <ul>
        <li><i class="fa fa-file-o"></i> SolutionB.sln</li>
        <li><i class="fa fa-file-o"></i> SolutionB.vs12.suo</li>
        <li><i class="fa fa-folder-o"></i> SolutionA
          <ul>
            <li><i class="fa fa-file-o"></i> SolutionA.csproj</li>
          </ul>
        </li>
        <li><i class="fa fa-folder-o"></i> SolutionA.Test
          <ul>
            <li><i class="fa fa-file-o"></i> SolutionA.Test.csproj</li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
</figure>

This leaves us with quite a lot of work as we still need to rename the solution directory and the individual projects, and we need to change the root namespace. 

Renaming the solution directory requires us to 1) Unload the solution from Visual Studio, 2) Rename the solution directory and c) Open the solution in Visual Studio. Now we have:

<figure>
  <ul class="file-list">
    <li><i class="fa fa-folder-o"></i> SolutionB
      <ul>
        <li><i class="fa fa-file-o"></i> SolutionB.sln</li>
        <li><i class="fa fa-file-o"></i> SolutionB.vs12.suo</li>
        <li><i class="fa fa-folder-o"></i> SolutionA
          <ul>
            <li><i class="fa fa-file-o"></i> SolutionA.csproj</li>
          </ul>
        </li>
        <li><i class="fa fa-folder-o"></i> SolutionA.Test
          <ul>
            <li><i class="fa fa-file-o"></i> SolutionA.Test.csproj</li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
</figure>

Renaming each of the projects in the solution using the rename feature in Visual Studio will only change the name of the project file and not the project directory and the root namespace. Leaving us with:

<figure>
  <ul class="file-list">
    <li><i class="fa fa-folder-o"></i> SolutionB
      <ul>
        <li><i class="fa fa-file-o"></i> SolutionB.sln</li>
        <li><i class="fa fa-file-o"></i> SolutionB.vs12.suo</li>
        <li><i class="fa fa-folder-o"></i> SolutionA
          <ul>
            <li><i class="fa fa-file-o"></i> SolutionB.csproj</li>
          </ul>
        </li>
        <li><i class="fa fa-folder-o"></i> SolutionA.Test
          <ul>
            <li><i class="fa fa-file-o"></i> SolutionB.Test.csproj</li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
</figure>

We now have two options for renaming the project directories:

Option A: 

1. Remove the projects from the solution
2. Rename the project directories
3. Add the projects to the solution
4. Add any missing project references

Option B:

1. Unload the solution
2. Rename the project directories
3. Find/Replace "SolutionA" with "SolutionB" in the solution file
4. Open the solution in Visual Studio

Neither of which are very elegant, but both will work. This gives us the following result:

<figure>
  <ul class="file-list">
    <li><i class="fa fa-folder-o"></i> SolutionB
      <ul>
        <li><i class="fa fa-file-o"></i> SolutionB.sln</li>
        <li><i class="fa fa-file-o"></i> SolutionB.vs12.suo</li>
        <li><i class="fa fa-folder-o"></i> SolutionB
          <ul>
            <li><i class="fa fa-file-o"></i> SolutionB.csproj</li>
          </ul>
        </li>
        <li><i class="fa fa-folder-o"></i> SolutionB.Test
          <ul>
            <li><i class="fa fa-file-o"></i> SolutionB.Test.csproj</li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
</figure>

Now we are almost done. The only thing left to do is to refactor the source code to use the new root namespace. One way to do this is to use find/replace in all source files, another is to use refactoring features of Visual Studio or Resharper.

But, how can we automate all of this? 

## Solution
PowerShell script that automates the entire process by:

1. Refactoring the root namespace (.sln, .suo, .csproj, .cs, .config)
2. Renaming the project files (.csproj)
3. Renaming the project directories
4. Renaming the solution files (.sln, .suo)
5. Renaming the solution directory

### Download
Download <a href="Rename-Visual-Studio-Solution.ps1">Rename-Visual-Studio-Solution.ps1</a> and put it someplace included in your environment %PATH%.

### Usage
a) Open PowerShell, b) navigate to the directory one level above your solution directory and c) invoke the script:

<code class="powershell">PS C:\Projects> Rename-Visual-Studio-Solution.ps1 SolutionA SolutionB</code>

### Script
We start by creating a PowerShell script Rename-Visual-Studio-Solution.ps1 that takes two parameters. The original root namespace and the new root namespace

<pre>
<code class="powershell"> param (
    [string]$originalRootNamespace,
    [string]$newRootNamespace
 )
</code>
</pre>

We cache all solution files, project files and source files in local variables.

<pre>
<code class="powershell">$sourceFiles = Get-ChildItem $originalRootNamespace -Include @("*.sln", "*.suo", "*.csproj", "*.nuspec", "*.cs", "*.config") -Recurse -Force
$projectFiles = Get-ChildItem $originalRootNamespace -Include @("*.csproj") -Recurse -Force
$solutionFiles = Get-ChildItem $originalRootNamespace -Include @("*.sln", "*.suo") -Recurse -Force
</code>
</pre>

#### Refactoring the root namespace
We iterate over the source files and replace the original root namespace with the new root namespace, using the <code>Get-Content</code>, <code>Foreach-Object</code> and <code>Set-Content</code> cmdlets.

<pre>
<code class="powershell">foreach ($sourceFile in $sourceFiles)
{
   (Get-Content $sourceFile.PSPath) | Foreach-Object { $_ -Replace $originalRootNamespace, $newRootNamespace } | Set-Content $sourceFile.PSPath
}
</code>
</pre>

This method will be sufficient in most cases, but since we are not using contextual refactoring tools there may be some scenarios in which it won't work. So change the method as appropriate.

#### Renaming the project files
We iterate over the project files and rename them one by one using the <code>Rename-Item</code> cmdlet.

<pre>
<code class="powershell">foreach ($projectFile in $projectFiles)
{
    $projectFileName = $projectFile.Name
    $newProjectFileName = $projectFileName.Replace($originalRootNamespace, $newRootNamespace)

    if ($projectFileName -ne $newProjectFileName) 
    {
        Rename-Item $projectFile $newProjectFileName
    }
}
</code>
</pre>

#### Renaming the project directories
We iterate over the project files again and rename each of the containing project directories, again using the <code>Rename-Item</code> cmdlet.

<pre>
<code class="powershell">foreach ($projectFile in $projectFiles)
{
    $projectDirectory = $projectFile.Directory
    $projectDirectoryName = $projectFile.Directory.Name
    $newProjectDirectoryName = $projectDirectoryName.Replace($originalRootNamespace, $newRootNamespace)

    if ($projectDirectoryName -ne $newProjectDirectoryName) 
    {
        Rename-Item $projectDirectory $newProjectDirectoryName
    }    
}
</code>
</pre>

#### Renaming the solution files
We iterate over the solution files rename each of them, again using the <code>Rename-Item</code> cmdlet.

<pre>
<code class="powershell">foreach ($solutionFile in $solutionFiles)
{
    $solutionFileName = $solutionFile.Name
    $newSolutionFileName = $solutionFileName.Replace($originalRootNamespace, $newRootNamespace)

    if ($solutionFileName -ne $newSolutionFileName) 
    {
        Rename-Item $solutionFile $newSolutionFileName
    }
}
</code>
</pre>

#### Renaming the solution directory
Finally, we rename the solution directory, and again using the <code>Rename-Item</code> cmdlet.

<pre>
<code class="powershell">$solutionDirectoryName = $originalRootNamespace
$newSolutionDirectoryName = $newRootNamespace

if ($solutionDirectoryName -ne $newSolutionDirectoryName) 
{
    Rename-Item $solutionDirectoryName $newSolutionDirectoryName
}
</code>
</pre>

Best regards,
Jonatan Pedersen