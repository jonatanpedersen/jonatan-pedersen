 param (
    [string]$originalRootNamespace,
    [string]$newRootNamespace
 )

$sourceFiles = Get-ChildItem $originalRootNamespace -Include @("*.sln", "*.suo", "*.csproj", "*.nuspec", "*.cs", "*.config") -Recurse -Force
$projectFiles = Get-ChildItem $originalRootNamespace -Include @("*.csproj") -Recurse -Force
$solutionFiles = Get-ChildItem $originalRootNamespace -Include @("*.sln", "*.suo") -Recurse -Force


# Replace Root Namespace in Files
foreach ($sourceFile in $sourceFiles)
{
   (Get-Content $sourceFile.PSPath) | Foreach-Object { $_ -Replace $originalRootNamespace, $newRootNamespace } | Set-Content $sourceFile.PSPath
}

# Rename Project Files
foreach ($projectFile in $projectFiles)
{
    $projectFileName = $projectFile.Name
    $newProjectFileName = $projectFileName.Replace($originalRootNamespace, $newRootNamespace)

    if ($projectFileName -ne $newProjectFileName) 
    {
        Rename-Item $projectFile $newProjectFileName
    }
}

# Rename Project Directories
foreach ($projectFile in $projectFiles)
{
    $projectDirectory = $projectFile.Directory
    $projectDirectoryName = $projectFile.Directory.Name
    $newProjectDirectoryName = $projectDirectoryName.Replace($originalRootNamespace, $newRootNamespace)

    if ($projectDirectoryName -ne $newProjectDirectoryName) 
    {
        Rename-Item $projectDirectory $newProjectDirectoryName
    }    
}

# Rename Solution Files
foreach ($solutionFile in $solutionFiles)
{
    $solutionFileName = $solutionFile.Name
    $newSolutionFileName = $solutionFileName.Replace($originalRootNamespace, $newRootNamespace)

    if ($solutionFileName -ne $newSolutionFileName) 
    {
        Rename-Item $solutionFile $newSolutionFileName
    }
}

# Rename Solution Directory
$solutionDirectoryName = $originalRootNamespace
$newSolutionDirectoryName = $newRootNamespace

if ($solutionDirectoryName -ne $newSolutionDirectoryName) 
{
    Rename-Item $solutionDirectoryName $newSolutionDirectoryName
}