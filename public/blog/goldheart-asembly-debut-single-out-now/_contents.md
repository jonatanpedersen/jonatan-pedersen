<img src="goldheart-assembly.jpg" alt="Goldheart Assembly - So Long St. Christopher Single">

It is finally available in stores, the melodiously captivating indie rock debut single from [Goldheart Assembly](http://www.facebook.com/pages/Goldheart-Assembly/14412758189) "So Long St. Christopher".

## Track-listing

* So Long St. Christopher (4:20)
* Oh Really (3:38)

Oh Really" was first aired live on BBC 6 Music from the Dublin Castle on the Friday of the 2009 Camden Crawl, and it was possibly one of the better acts that night.

So Long St. Christopher" went on to win the 6 Music Rebel Playlist of the week by a huge margin in mid May.<

A limited edition 7" is available for £3.49 at the [recordstore.co.uk](http://www.recordstore.co.uk/productdetail.jsp?productPK=unittest-wDaA97QOwKGu8oKqqN3IEb-1), and a 256 kbps AAC for £1.29 in the Itunes Music Store.

At these prices there really is no excuse not to try out this up and coming assembly.