Facebook has rolled-out their long awaited vanity URLs just a few hours ago. This allows users to register shortnames for use in their profile and page URLs, and is particularly useful when linking to your profile from limited services like Twitter, IM or text messages.

Simply go to http://www.facebook.com/username/ to register your username.

But choose carefully, once you have registered your username you can never change it.