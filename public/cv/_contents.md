# Jonatan Pedersen
Copenhagen, Feb 7th 2015

Postal address: Store Kongensgade 92B 1MF, 1264 København K, Danmark<br/>
Phone: 0045 53624511<br />
Email: <a href="mailto:jp@jonatanpedersen.com">jp@jonatanpedersen.com</a><br />
Website: <a href="http://www.jonatanpedersen.com">www.jonatanpedersen.com</a><br />
LinkedIn: <a href="https://www.linkedin.com/in/jonatanpedersen" title="">www.linkedin.com/in/jonatanpedersen</a>

## Profile
I am full-stack developer, architect, Product Owner and ScrumMaster with more than twelve years experience in the internet industry.

I have a great passion for technology, and I spend a great deal of my time studying software architecture, design patterns and best practices (SOLID, DDD, CQRS, pair programming), test automation (NUnit and Selenium), continuous integration (CruiseControl and Bamboo) and languages, frameworks, libraries and tools (JavaScript, HTML, CSS, C#, .NET, Bootstrap, Knockout, AngularJS, Node, Bower and Gulp to name a few).

I have worked in global conglomerates and tiny startups, and I have lived and worked in London for four years. I have worked Scrum projects as Product Owner for Yahoo! and Vodafone, and as Developer and ScrumMaster for Jobzonen, Edora and Capevo, and I have worked water fall projects as a Product Development Manager for Yahoo!, done market research, written product requirements and specifications.

## Experience
### Senior Developer and Scrum Coach
Capevo A/S (Copenhagen, Denmark)<br/>
_Aug 2014 - Present (7 months)_

#### Key Responsibilities:
* Develop a next generation data-collection and reporting platform for government institutions
* Coach the ScrumMaster and the team

#### Key Achievements:
* The team is highly self-organized, productive and happy. There is good collaboration all around, and the project is moving forward at an increasing pace

<hr class="page-break" />

### Senior Systems Developer and Architect
Edora A/S (Copenhagen, Denmark)<br/>
_Jan 2013 - Jul 2014 (1 Year, 7 months)_

#### Key Responsibilities:
* Develop booking solution for Arbejdsmarkedsstyrelsen and for Ældre Sagen

#### Key Achievements:
* Developed a kiosk solution that allow people to check-in, print receipt, reschedule or cancel appointments on arrival at a reception 

### Developer
Jobzonen A/S (Copenhagen, Denmark)<br/>
_May 2011 - Dec 2012 (1 Year, 8 months)_

#### Key Responsibilities:
* End to end development (design, implementation, test and release) of new products and features as well as maintenance of the existing product portfolio
* Support the Product Manager with the implementation of Scrum

#### Key Achievements:
* Developed a new Jobzonen website with an improved user experience and modern technology stack
* Designed and implemented an enterprise search solution for job postings based on Ankiro Enterprise Suite

### Product Owner
Vodafone (Copenhagen, Denmark)<br/>
_September 2010 - May 2011 (9 Months)_

#### Key Responsibilities:
* Manage the Vodafone 360 Website product

#### Key Achievements:
* Developed and rolled-out a new registration flow that increased sign-up conversion
* Developed and rolled-out new features across 360 People, 360 Photos and 360 Email

### Director
Fugl Ltd. (London, England)<br/>
_December 2009 - September 2010 (10 Months)_

<hr class="page-break" />

### Product Development Manager
Yahoo!/Kelkoo (London, England)<br/>
_July 2006 – October 2009 (3 years 4 months)_

#### Key Responsibilities:
* Managed the development of key assets for Kelkoo, from requirements gathering, specification writing and prototyping to user acceptance testing, user training and product rollout. Worked closely with stakeholders, including users, business intelligence, product, marketing and engineering functions 

#### Key Achievements:
* Developed and rolled-out a new site-search ranking algorithm that increased revenues, and enhanced relevance and precision
* Developed and rolled-out a method for accurately tracking and analysing click-through data on search results
* Defined mid/long term strategy for building a sustainable eco-system for advertisers, publishers, developers and users while simultaneously attending to short term business requirements and deliveries
* Successfully assisted in transitioning the development team to an agile and value driven development methodology (Scrum)
* Developed and rolled-out the Kelkoo Partner Programme, the Shopping APIs Platform and the Kelkoo Developer Network across 10 markets in Europe
* Won the "Best shopping hack" award at a Yahoo! Europe hack-day event

### Content Development Manager
Yahoo!/Kelkoo (Copenhagen, Denmark)<br/>
_June 2004 – June 2006 (2 years 1 month)_

#### Key Responsibilities:
* Managed the development of content, categories and product information on the Kelkoo website in Denmark
* Defined content strategy together with the Product Manager

#### Key Achievements:
* Managed product information for over 200 shopping related categories
* Developed internationalization quality assurance system for translating and tracking quality of product information across Europe
* Developed task management and reporting system
* Developed content management system
* Managed the day to day activities of two part-time contractors
* Web Developer

<hr class="page-break" />

### Self-employed and short-term contracts (Hillerød, Denmark)<br/>
_October 1999 - May 2004 (4 years 8 months)_

#### Key Achievements:
* Developed product labelling system for peripherals manufacturer Sandberg A/S
* Developed intranet for computer supplier Bluecom Danmark A/S
* Developed website for Four Fashion and Four Records (Bluecom Danmark A/S)
* Developed website for local carpenter Allan Svendsen & Søn A/S
* Developed website for the Danish political party Social-Liberal Youth
* Developed website for real-estate company Dansk Restaurations Handel ApS
* Developed online shop for children's fashion designer Kools A/S

## Education
### Secondary School
Erhvervsskolen Nordsjælland (Hillerød, Denmark)<br/>
_August 1999 – June 2002 (2 years 11 month)_

* Multimedia and Communication (A level)
* Danish (A level)
* Math (A level)
* Computer Science (B level)
* English (B level)
* Physics (B level)
* Biology (B level)
* Computer Programming (C level)