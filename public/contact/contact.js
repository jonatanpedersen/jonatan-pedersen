(function(angular, undefined) {
	angular
		.module('contact', [])
		.controller('ContactController', ['$scope', '$http', function($scope, $http) {
			$scope.submit = function() {
				$http({
					method: 'post',
					url: '/api/contact'
				});
			};
		}]);
})(angular);