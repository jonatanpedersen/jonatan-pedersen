var gulp = require('gulp'),
    extend = require('gulp-extend'),
    jeditor = require('gulp-json-editor'),
    request = require('request'),
    shell = require('gulp-shell'),
    source = require('vinyl-source-stream'),
    streamify = require('gulp-streamify');


gulp.task('harp', shell.task([
    'harp compile'
]));

gulp.task('github', function () {
    return request({
            url: 'https://api.github.com/users/jonatanpedersen/repos',
            headers: {
                'User-Agent': 'request'
            }
        })
        .pipe(source('_github.json'))
        .pipe(streamify(jeditor(function (repositories) {
            var data = {
                'profile': {
                    'repositories': []
                }
            };
            repositories.forEach(function (repo) {
                data.profile.repositories.push({
                    html_url: repo.html_url,
                    name: repo.name,
                    language: repo.language,
                    description: repo.description
                });
            });
            return data;
        })))
        .pipe(gulp.dest('./public'));
});

gulp.task('public-data', ['github'], function () {
    return gulp.src('./public/_*.json')
        .pipe(extend('_data.json', true, '    '))
        .pipe(gulp.dest('./public'));
});

gulp.task('default', ['github', 'public-data']);
